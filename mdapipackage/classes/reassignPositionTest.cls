@isTest
public class reassignPositionTest {
    @isTest public static void Test(){
		User u = TestDataFactory.createUser();
        System.runAs(u){
            List<Position__c> pos = TestDataFactory.createPositions(1, 'TestTitle');
            Position__c pos2 = [SELECT OwnerId FROM Position__c WHERE Name='TestTitle'];        
            System.debug('In Test Position after insert ' + pos2);
            System.assertEquals([SELECT QueueId from QueueSObject WHERE Queue.Name = 'Unclaimed Positions' and  Queue.Type ='Queue' LIMIT 1].QueueId, pos2.OwnerId);
      }
    }
}