@isTest
public class createTasksByStatusTest {
    @testSetup public static void setup(){
        List<Candidate__c> candidates = TestDataFactory.createCandidates(2);
       	List<Position__c> positions = TestDataFactory.createPositions(1, 'TestTitle');
        List<Job_Application__c> jobApplications = TestDataFactory.createJobApplications(2, candidates, positions.get(0));
    }
    @isTest public static void testRejectionTask(){
		List<Job_Application__c> applications = [SELECT Id FROM Job_Application__c];
        for(Job_Application__c item : applications){
            item.Status__c = 'Rejected';
        }
        update applications;
        List<Task> createdTasks = [SELECT Id, Subject FROM Task];
        System.assertEquals(2, createdTasks.size());
        System.assertEquals('Send Rejection Letter', createdTasks.get(0).Subject);
    }
    
    @isTest public static void checkException(){
        //createTaskByStatusHelper.InCatch = true;
    }
    
    @isTest public static void testExtendAnOfferTask(){
		List<Job_Application__c> applications = [SELECT Id FROM Job_Application__c];
        for(Job_Application__c item : applications){
            item.Status__c = 'Extend an Offer';
        }
        update applications;
        List<Task> createdTasks = [SELECT Id, Subject FROM Task];
        System.assertEquals(2, createdTasks.size());
        System.assertEquals('Extend an Offer', createdTasks.get(0).Subject);
    }
}