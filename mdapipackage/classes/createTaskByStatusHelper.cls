public class createTaskByStatusHelper {
    @testVisible static Boolean InCatch = false;
    public static task createDefaultTask(Id OwnerId, Id WhatId, String Subject){ 
        Task task = new Task();
        task.OwnerId = OwnerId;
        task.Subject = Subject;
        task.Status = 'Not Started';
        task.Priority = 'High';
        task.WhatId = WhatId;
        return task;
    }
}