trigger sendEmailAfterPosCreation on Position__c (before insert) {
    if(Trigger.IsBefore){
        if(Trigger.isInsert){
            for(Position__c pos : trigger.new){
                if(EmailService.sendEmail(pos)){
                    System.debug('Success');
                    pos.Recruiters_are_notified__c = true;
                }
            }
            
        }
    }
}