trigger reassignPosition on Position__c (before insert) {
    if(Trigger.IsBefore){
        if(Trigger.IsInsert){
            List<QueueSObject> queue = [SELECT Id, QueueId from QueueSObject WHERE Queue.Name = 'Unclaimed Positions' LIMIT 1];
            List<User> notRecruiters = [SELECT Id from user WHERE UserRole.Name !='Recruiter' AND UserRole.Name !='Recruiting Manager'];
            List<Id> IdOfNotRecruiters = new List<Id>();
            if(notRecruiters.size() > 0){
                for(User u : notRecruiters){
                    IdOfNotRecruiters.add(u.Id);
                }
            }
            for(Position__c pos : Trigger.new){
                //List<User> role = [Select  UserRole.Name from user WHERE Id = :pos.OwnerId];
                if(IdOfNotRecruiters.size() > 0 && IdOfNotRecruiters.contains(pos.OwnerId)){
                    if(queue.size() > 0){
                        pos.OwnerId = queue.get(0).QueueId;
                    }
                }
            }
        }
    }
}