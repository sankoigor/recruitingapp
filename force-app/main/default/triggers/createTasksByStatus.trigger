trigger createTasksByStatus on Job_Application__c (after update) {
    if(Trigger.IsAfter){
        if(Trigger.IsUpdate){
            List<Task> tasks = new List<Task>(); 
            for(Job_Application__c app : Trigger.new){
                if(app.Status__c == 'Rejected' && Trigger.OldMap.get(app.Id).Status__c != Trigger.NewMap.get(app.Id).Status__c){
                    tasks.add(createTaskByStatusHelper.createDefaultTask(app.OwnerId, app.Id, 'Send Rejection Letter'));                
                }
                 if(app.Status__c == 'Extend an Offer' && Trigger.OldMap.get(app.Id).Status__c != Trigger.NewMap.get(app.Id).Status__c){
                     tasks.add(createTaskByStatusHelper.createDefaultTask(app.OwnerId, app.Id, 'Extend an Offer'));       
                 }
            }
            try{
            insert tasks;
            }
            catch(DMLException e){
                System.debug(e); 
            }
        }
    }
}