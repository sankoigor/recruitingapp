@isTest
public class reassignPositionTest {
    
    @isTest public static void Test(){
		User u = TestDataFactory.createUser();
        System.runAs(u){
            Id QueueId = getQueueId();
            List<Position__c> pos = TestDataFactory.createPositions(1, 'TestTitle');
            List<Position__c> pos2 = [SELECT OwnerId FROM Position__c WHERE Name='TestTitle'];
            System.assertEquals(QueueId, pos2.get(0).OwnerId);
      }
    }
    private static Id getQueueId(){
        List<QueueSObject> queues = [SELECT Id, QueueId from QueueSObject WHERE Queue.Name = 'Unclaimed Positions' LIMIT 1];
        Id QueueId = null;
        if(queues.size() > 0){
            QueueId = queues.get(0).QueueId;
        }
        else{
             Group g1 = new Group(Name='Unclaimed Positions', type='Queue');
             insert g1;
             QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Position__c');
             insert q1;
            List<QueueSObject> queues2 = [SELECT Id, QueueId from QueueSObject WHERE Queue.Name = 'Unclaimed Positions' LIMIT 1];
            QueueId = queues2.get(0).QueueId;
			//QueueId = q1.Id;
        }
        return QueueId;
    }
}