public class EmailService {
    public static Boolean sendEmail(Position__c pos){
        System.debug('In HELPER');
        List<String> roles = new List<String>{'Recruiter', 'Recruiting Manager'};
        List<User> users = [Select Email from user WHERE UserRole.Name IN :roles];
        if(users.size() > 0){
            List<String> emails = new List<String>();
            for(User user : users){
                emails.add(user.Email);
            }
            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = emails;
            message.setSenderDisplayName('Salesforce Support');
            message.subject = 'Position' + pos.Name + ' is created';
            message.plainTextBody = 'Position' + pos.Name + 'is created';
            Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message });
            if (results[0].success) {
                System.debug('The email was sent successfully.');
                return true;
            } else {
                System.debug('The email failed to send: ' + results[0].errors[0].message);
                
            }
        }
        return false;
    }
}