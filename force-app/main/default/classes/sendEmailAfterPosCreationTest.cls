@isTest
public class sendEmailAfterPosCreationTest {
    @testSetup public static void setup(){
        List<UserRole> roles = new List<UserRole>();
        UserRole r = new UserRole(DeveloperName = 'Recruiter', Name = 'Recruiter');
        UserRole r2 = new UserRole(DeveloperName = 'RecruitingManager', Name = 'Recruiting Manager');
		roles.add(r);
        roles.add(r2);
        insert roles;
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
        User u1 = TestDataFactory.createUser('Recruiter');
        User u2 = TestDataFactory.createUser('Recruiting Manager');
        }
    }
    
    @isTest public static void testEmails(){ 
       	List<Position__c> pos = TestDataFactory.createPositions(1, 'testTitle');
        Position__c pos2 = [SELECT OwnerId, Recruiters_are_notified__c FROM Position__c WHERE Name='testTitle'];
        System.assertEquals(1,Limits.getEmailInvocations());
        System.assertEquals(true,pos2.Recruiters_are_notified__c);
        
    }
    
}