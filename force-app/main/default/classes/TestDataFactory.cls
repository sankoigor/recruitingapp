@isTest
public class TestDataFactory {
    public static final Id SysAdminId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
    public static List<Candidate__c> createCandidates(Integer count){
        List<Candidate__c> candidates = new List<Candidate__c>();
        if(count >= 1){  
            for(Integer i = 1; i <= count; i++){
                candidates.add(new Candidate__c());
            }
            insert candidates;           
        }
        return candidates;
    }
    
    public static List<Position__c> createPositions(Integer count, String title){
        List<Position__c> positions = new List<Position__c>();
        if(count >= 1){
            for(Integer i = 1; i <= count; i++){
                positions.add(new Position__c(Name=title));
            }
            insert positions;    
        }
        return positions;
    }
    
    public static List<Job_Application__c> createJobApplications(Integer count, List<Candidate__c> candidates, Position__c position){
        List<Job_Application__c> jobApplications = new List<Job_Application__c>();
        if(count >= 1){
            for(Integer i = 1; i <= count; i++){
                jobApplications.add(new Job_Application__c(Candidate__c = candidates.get(i-1).Id, Position__c = position.Id));
            }
            insert jobApplications;    
        }
        return jobApplications;
    }
    
    public static User createUser(){
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
		insert r;
        User u = new User(
            ProfileId = SysAdminId,
            LastName = 'LastName',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );
        insert u;
        return u;
    }
    
    public static User createUser(String Role){
        User u = new User();
        List<UserRole> userRole = [select Id from UserRole WHERE Name = :Role ];      
        u.ProfileId = SysAdminId;
            u.LastName = 'LastName';
        u.Email = 'puser000@amamama.com';
        u.Username = 'puser000@amamama.com' + System.currentTimeMillis();
        u.CompanyName = 'TEST';
        u.Title = 'title';
        u.Alias = 'alias';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        if(userRole.size() > 0 && userRole.get(0).Id != null){
            u.UserRoleId = userRole.get(0).Id;      
        }
        insert u;
        return u;
    }
    
    public static List<Account> createAccount(Integer count){
        List<Account> accounts = new List<Account>();
        if(count >= 1){
             for(Integer i = 1; i <= count; i++){
                accounts.add(new Account(Name = 'TestAccount'+i));
            }
            insert accounts;
        }
        return accounts;
    }
    
    public static List<Contact> createContact(Integer count, Id AccId){
        List<Contact> contacts = new List<Contact>();
        if(count >= 1){
             for(Integer i = 1; i <= count; i++){
                contacts.add(new Contact(LastName = 'TestLastName'+i, AccountId = AccId));
            }
            insert contacts;
        }
        return contacts;
    }
    
    public static List<Opportunity> createOpportunity(Integer count, Id AccId){
        List<Opportunity> opportunities = new List<Opportunity>();
        if(count >= 1){
             for(Integer i = 1; i <= count; i++){
                opportunities.add(new Opportunity(Name = 'OppName'+i, AccountId = AccId, CloseDate=System.today(), StageName='Prospecting'));
            }
            insert opportunities;
        }
        return opportunities;
    }
    
    public static List<Lead> createLead(Integer count){
        List<Lead> leads = new List<Lead>();
        if(count >= 1){
             for(Integer i = 1; i <= count; i++){
                leads.add(new Lead(LastName = 'LeadLastName'+i, Company='TestCompany', Status='	Open - Not Contacted'));
            }
            insert leads;
        }
        return leads;
    }
    
    public static List<SObject> createRecords(String objectName, Map<String, Object> fields, Integer count){
        List<SObject> objects = new List<SObject>();
        if(count >= 1){
            
            for(Integer i = 1; i <= count; i++){
        	sObject sObj = Schema.getGlobalDescribe().get(objectName).newSObject();
			Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
                System.debug(objectFields);
                for(Schema.SObjectField field : objectFields.values()) {
                    for(String sentField : fields.KeySet()){
                        if(String.valueOf(field) == sentField){
                            sObj.put(field, fields.get(sentField));                        
                                }
                    }
                }
            }
        }
        return objects;
    }
}